# YOLOv5 🚀 by Ultralytics, AGPL-3.0 license
"""
AutoAnchor utils

Adapted from https://github.com/WongKinYiu/yolov7/blob/main/utils/autoanchor.py commit 3708cde by Labra AI

Changelog:
1. Remove unnecessary functions / code for using with YOLOv5
2. Enable running as script, by passing width-height data as .npy file and dumping results to .npy file

This project is freely available at https://gitlab.com/labra-public/autoanchor with AGPL-3.0 license
"""

import random
import sys
import numpy as np
import torch
from tqdm import tqdm

PREFIX = 'AutoAnchor: '

def kmean_anchors(wh0, n=9, img_size=640, thr=4.0, gen=1000):
    """ Creates kmeans-evolved anchors from training dataset
        Arguments:
            dataset: path to data.yaml, or a loaded dataset
            n: number of anchors
            img_size: image size used for training
            thr: anchor-label wh ratio threshold hyperparameter hyp['anchor_t'] used for training, default=4.0
            gen: generations to evolve anchors using genetic algorithm
            verbose: print all results

        Return:
            k: kmeans evolved anchors

        Usage:
            from utils.autoanchor import *; _ = kmean_anchors()
    """
    from scipy.cluster.vq import kmeans

    npr = np.random
    thr = 1 / thr

    def metric(k, wh):  # compute metrics
        r = wh[:, None] / k[None]
        x = torch.min(r, 1 / r).min(2)[0]  # ratio metric
        return x, x.max(1)[0]  # x, best_x

    def anchor_fitness(k):  # mutation fitness
        _, best = metric(torch.tensor(k, dtype=torch.float32), wh)
        return (best * (best > thr).float()).mean()  # fitness

    def sort_k(k):
        k = k[np.argsort(k.prod(1))]  # sort small to large
        return k

    # Filter
    i = (wh0 < 3.0).any(1).sum()
    if i:
        print(f'{PREFIX}WARNING ⚠️ Extremely small objects found: {i} of {len(wh0)} labels are <3 pixels in size')
    wh = wh0[(wh0 >= 2.0).any(1)].astype(np.float32)  # filter > 2 pixels
    # wh = wh * (npr.rand(wh.shape[0], 1) * 0.9 + 0.1)  # multiply by random scale 0-1

    # Kmeans init
    try:
        print(f'{PREFIX}Running kmeans for {n} anchors on {len(wh)} points...')
        assert n <= len(wh)  # apply overdetermined constraint
        s = wh.std(0)  # sigmas for whitening
        k = kmeans(wh / s, n, iter=30)[0] * s  # points
        assert n == len(k)  # kmeans may return fewer points than requested if wh is insufficient or too similar
    except Exception:
        print(f'{PREFIX}WARNING ⚠️ switching strategies from kmeans to random init')
        k = np.sort(npr.rand(n * 2)).reshape(n, 2) * img_size  # random init
    wh, wh0 = (torch.tensor(x, dtype=torch.float32) for x in (wh, wh0))
    k = sort_k(k)


    # Evolve
    f, sh, mp, s = anchor_fitness(k), k.shape, 0.9, 0.1  # fitness, generations, mutation prob, sigma
    pbar = tqdm(range(gen))  # progress bar
    for _ in pbar:
        v = np.ones(sh)
        while (v == 1).all():  # mutate until a change occurs (prevent duplicates)
            v = ((npr.random(sh) < mp) * random.random() * npr.randn(*sh) * s + 1).clip(0.3, 3.0)
        kg = (k.copy() * v).clip(min=2.0)
        fg = anchor_fitness(kg)
        if fg > f:
            f, k = fg, kg.copy()
            pbar.desc = f'{PREFIX}Evolving anchors with Genetic Algorithm: fitness = {f:.4f}'


    return sort_k(k).astype(np.float32)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_path', type=str, required=True, help='Width-height array path, e.g. `/tmp/data.npy`')
    parser.add_argument('--dump_path', type=str, required=True, help='Output path for anchor data file, in .npy format')
    parser.add_argument('--n', type=int, default=9, help='number of anchors')
    parser.add_argument('--img_size', type=int, required=True, help='image size used for training')
    parser.add_argument('--thr', type=float, default=4.0, help='anchor-label wh ratio threshold')
    parser.add_argument('--gen', type=int, default=1000, help='generations to evolve anchors using genetic algorithm')
    opt = parser.parse_args()

    # Load width/height array
    wh0 = np.load(opt.data_path)  # wh0.shape = (n, 2)
    anchors = kmean_anchors(wh0, n=opt.n, img_size=opt.img_size, thr=opt.thr, gen=opt.gen)

    # Save anchors
    np.save(opt.dump_path, anchors)
    print(f'{PREFIX}Saved anchors to {opt.dump_path}')

    sys.stdout.flush()
